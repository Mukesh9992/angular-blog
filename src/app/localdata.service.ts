import { Injectable } from '@angular/core';
import { InMemoryDbService } from 'angular-in-memory-web-api';

@Injectable()
export class LocaldataService implements InMemoryDbService {
  createDb() {
    const loginContent = [{
      username: 'user1',
      password: 'user1'
    },
    {
      username: 'user2',
      password: 'user2'
    },
    {
      username: 'user3',
      password: 'user3'
    },
    {
      username: 'user4',
      password: 'user4'
    },
    {
      username: 'user5',
      password: 'user5'
    },
    {
      username: 'user6',
      password: 'user6'
    }]
    return {'loginContent':loginContent };
  }
}