import { Component, OnInit,EventEmitter,Input,Output } from '@angular/core';

@Component({
  selector: 'app-input',
  templateUrl: './input.component.html',
  styleUrls: ['./input.component.css']
})
export class InputComponent implements OnInit {
  @Input() label:string;
  @Input() id:string;
  @Input() type:string;
  @Input() ivalue:string;
 @Output() ivaluechange=new EventEmitter();
  constructor() {
    
   }
  change(event){
    var obj={};
    obj[event.currentTarget.id]=this.ivalue
    this.ivaluechange.emit(obj)
  }
  ngOnInit() {
  }
  
}
